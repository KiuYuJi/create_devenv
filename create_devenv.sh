sudo apt-get install -y git git-flow zsh tmux libfreetype6-dev libjpeg-dev python-dev libmysqlclient-dev mysql-server-5.6
pip install virtualenv virtualenvwrapper
git clone https://github.com/robbyrussell/oh-my-zsh.git
sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
echo "export WORKON_HOME=$HOME/.virtualenvs" >> ~/.zshrc
echo "source /usr/local/bin/virtualenvwrapper.sh" >> ~/.zshrc
